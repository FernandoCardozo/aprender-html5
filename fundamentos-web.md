# Los fundamentos de la web

En este siguiente apartado voy a compartir lo escencial que debo aprender para el desarrollo web. Desde cómo funciona la web hasta qué es el lado del cliente, protocolos, tecnologías y más.

## ¿Cómo funciona la web?

En este apartado mostraré lo básico de qué es Internet y qué es la web, incluso describir la diferencia entre ambos porque hay personas que confunden ambos términos.

### ¿Qué es **Internet**?

Es *un sistema descentralizado de redes de ordenadores conectadas entre sí*, distribuidas a nivel mundial, que ofrece servicios de comunicación de datos. Anteriormente fue llamado **ARPANET**.

### ¿Qué es la **web**?

Es *un sistema de distribución y recuperación de documentos* basados en hipertexto.

### ¿Cuál es la diferencia entre Internet y la web?

Básicamente **Internet** es una infraestructura en donde se aloja diferentes servicios y la **web** es uno de los tantos que existen en el mismo.

## El paradigma Cliente/Servidor

El cliente es nuestro computador, el servidor es otro computador que está alojado en otra parte y que funciona prácticamente todo el día, siendo así que aloja todo el sitio web o aplicación web.

El **cliente** envía una petición a un servidor de qué contenido necesita, el **servidor** simplemente lo que hace es escuchar y devolver una respuesta a esa petición. Aunque dependiendo de la aplicación el servidor hará una verificación en la base de datos si los datos que pide el cliente serán correctos o tiene los permisos para mostrárselos.

Existen muchos protocolos que sirven para transferir archivos entre un cliente y un servidor, son como reglas que tienen que seguir para su correcto funcionamiento.

## ¿Qué es IP y DNS?

Las direcciones **IP** son identificadores numéricos, de manera lógica y jerárquica, a un dispositivo en la red que utilice el *protocolo IP*.

Las direcciones IP se podrían ver algo como esto: **86.75.20.654**

El problema está en que, si no hubiera existido las DNS, tendríamos que utilizar las direcciones IP en vez de nombres de dominios que nos facilitan hoy en día recordar aplicaciones o sitios web.

Las **DNS** son sistemas de nomenclatura jerárquicas para computadoras, servicios o cualquier recurso conectado a Internet. Gracias a esto nos proporcionará una equivalencia de los dominios con las direcciones IP.

**Las IP estáticas y las IP dinámicas**

Las *IP estáticas* no cambian en la mayor parte del tiempo y es ideal para aplicaciones web o sitios web. Por otro lado tenemos a las *IP dinámicas* que cambian cuando encendemos un router, por ejemplo.

## ¿Qué son los protocolos?

Los protocolos son *un conjunto de normas para el intercambio de información* entre dos o más dispositivos.

Hay dos protocolos que son bastantes importantes que son: **TCP/IP**

El TCP es el *protocolo de control de transmisión.* y el IP es el *protocolo de Internet*.

## ¿Qué es el protocolo HTTP?

El protocolo HTTP es un *protocolo de transferencia de hipertexto*, es muy popular porque se utiliza para acceder a las páginas web.

## Partes de una URL

Una URL es un *localizador uniforme de recursos* que sirve para **nombrar los recursos en Internet**. Esta denominación tiene un formato estándar y su propósito es asignar una dirección única para cada uno de los recursos disponibles en Internet.

Analizamos las partes de una URL: https://www.wikipedia.org/

- El **https** es el protocolo utilizado por el sitio o aplicación.
- El **://** es simplemente una separación del protocolo con los recursos. Nada de especial.
- Las **www** hacen referencia a un subdominio.
- La palabra **wikipedia** es un dominio de segundo nivel.
- La palabra **org** es un dominio de primer nivel.

El dominio de segundo y primer nivel conforman el dominio, es el nombre que utiliza el navegador para *localizar* el servidor.

## ¿Cómo funcionan los navegadores?

Los navegadores simplemente nos proporcionan una entrada para la web, para buscar informaciones sobre páginas, sitios o aplicaciones web. Los navegadores tienen algunos componentes en común.

- Motor interno
- Motor de renderizado
- Los protocolos de comunicaciones
- El almacenamiento de datos
- Intérprete de JavaScript