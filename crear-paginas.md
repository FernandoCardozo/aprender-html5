# ¿Cómo se crean las páginas web?

En este apartado voy a comentar y explicar cómo se crean las páginas web para entender aún mejor este mundo de la dichosa web.

## ¿Qué es la web?

La web es, básicamente, un conjunto de documentos que se conectan a través de hipervínculos. Con ello se ha creado el lenguaje de marcado HTML, que permite crear documentos y crear enlaces entre ellos. Dicho todo esto se necesitará de un navegador que permite acceder a la web y, con ello, a los documentos que componen la aplicación o el sitio web especificado.

## Las tecnologías estándares de la web

- HTML: Lenguaje de marcado de hipertexto
- CSS: Hojas de estilo en cascada
- JavaScript: Lenguaje de programación de la web

## Las diferencias entre...

- Páginas web: Simplemente es un documento HTML que puede o no contener CSS y JavaScript.
- Sitios web: Son varias páginas web estructuradas bajo un dominio.
- Aplicaciones web: Son programas completos con bases de datos y lógica.

## ¿Qué es el hosting y el dominio?

El **dominio** es el nombre que tendrá tu marca o proyecto en la web, en la cual las personas lo identificarán y lo reconocerán colocando en la dirección de URL el nombre del dominio para acceder a tu sitio o aplicación web.

El **hosting** es un computador en donde alojará todos tus archivos de tu aplicación o sitio web.

## ¿Por qué elegir el desarrollo web?

- Porque es multiplataforma
- Es estándar en la gran mayoría de navegadores
- Tiene costos mínimos de entrada