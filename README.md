# El aprendizaje desde cero de HTML5

Mi objetivo principal es aprender sobre el lenguaje de marcado HTML5, siendo así para emprender en el camino de ser un programador y maquetador web. *Próximamente aprenderé* nuevas tecnologías como CSS, JavaScript y muchas más.

## ¿Qué finalidad tiene este repositorio?

Nada. Simplemente es un repositorio común que mostraré al público sobre mis progresos básicos en HTML y que posiblemente le agregue, más adelante, CSS y JavaScript puro.

**Espero que disfruten de este simple repositorio, no tengo nada más de qué comentar.**

Icono diseñado por <a href="https://www.flaticon.es/autores/prosymbols" title="Prosymbols">Prosymbols</a> de <a href="https://www.flaticon.es/" title="Flaticon"> www.flaticon.es</a>